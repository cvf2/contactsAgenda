import { Component, OnInit  } from '@angular/core';

@Component({
  selector: 'contact-app',
  templateUrl: './contact-app.component.html',
  styleUrls: ['./contact-app.component.scss']
})
export class ContactAppComponent implements OnInit {
  title: string; 
  
	constructor(){ 
		this.title = 'Carlos Vega Fernandez';
  }	
  
  ngOnInit() {
    }

}
