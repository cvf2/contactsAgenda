import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Componentes
import { AppComponent } from './app.component';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactItemComponent } from './contact-item/contact-item.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactAppComponent } from './contact-app/contact-app.component';

//Modulos
import { ContactService } from './services/contactService';


@NgModule({
  declarations: [
    AppComponent,
    ContactAddComponent,
    ContactListComponent,
    ContactItemComponent,
    ContactDetailComponent,
    ContactAppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ContactService],
  bootstrap: [ContactAppComponent]
})
export class AppModule { }
