import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../model/contact';

@Component({
  selector: 'contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {

  id:number;
  firstNameTxt:string;
  lastNameTxt:string;

  @Input() contact: Contact;

  constructor() {
  }

  ngOnInit() {
  }

  deleteContact() {
    
  }

  goDetail() {

  }

}
