import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../model/contact';
import { ContactService } from '../services/contactService';

@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  contacts: Contact[];
  title_list: string;

  constructor(private _contactService:ContactService) {
    this.contacts = this._contactService.getContacts();
  }

  ngOnInit() {
    this.title_list = "Listado de contactos"
  }

  

}
