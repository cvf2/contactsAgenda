import { Injectable } from '@angular/core';
import { Contact } from '../model/contact';

@Injectable()
export class ContactService {

  contacts: Contact[];
  id: number = 1;

  constructor() { }

  getContacts(): Contact[] {
    return [
      { firstName: "John", lastName: "Doe", id: 0, phoneNumber: 1 },
      { firstName: "John", lastName: "Doe1", id: 0, phoneNumber: 2 },
      { firstName: "John", lastName: "Doe2", id: 0, phoneNumber: 3 }
    ]

  }

  deleteContact(id: number) {
    this.contacts = this.contacts.filter(item => item.id !== id);
  }

  addContact(firstName: string, lastName: string, phoneNumber: number) {
    if (this.contacts != null) {
      var contactAdd = new Contact();
      contactAdd.id = this.id;
      this.id++;  //Incrementamos el Id.
      contactAdd.firstName = firstName;
      contactAdd.lastName = lastName;
      contactAdd.phoneNumber = phoneNumber;
      this.contacts.push(contactAdd);
    }
  }

}
